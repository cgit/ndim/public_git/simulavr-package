# Only build noarch subpackages if distribution supports it
%if 0%{?fedora} >= 11
%global noarch_subpackage BuildArch: noarch
%else
%global noarch_subpackage %{nil}
%endif


Name:		simulavr
Version:	0.1.2.6
Release:	5%{?dist}
Summary:	An AVR Simulator

Group:		Development/Tools
License:	GPLv2+
URL:		http://savannah.nongnu.org/projects/simulavr/
Source0:	http://download.savannah.nongnu.org/releases/simulavr/%{name}-%{version}.tar.gz
# Allow overriding ENABLE_WARNINGS definition
Patch100:	0001-Allow-overriding-ENABLE_WARNINGS.patch
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildRequires:	avr-binutils
BuildRequires:	avr-gcc
BuildRequires:	avr-libc
BuildRequires:	bison
BuildRequires:	doxygen
BuildRequires:	flex
BuildRequires:	ncurses-devel
BuildRequires:	python
BuildRequires:	texi2html
BuildRequires:	texinfo
BuildRequires:	texinfo-tex


%description
A simulator for microcontrollers from Atmel's AVR series.


%package doc
Group:		Development/Tools
Summary:	Documentation for simulavr (HTML and PDF)
%{noarch_subpackage}


%description doc
A simulator for microcontrollers from Atmel's AVR series.

This subpackage contains the documentation in HTML and PDF format.


%prep
%setup -q
%patch100 -p1 -b .override-enable-warnings


%build
%configure --enable-pdf
# FIXME: Skip -Werror for now, as flex generated code ignores
# fwrite(3) return value which generates a warning.
make %{?_smp_mflags} ENABLE_WARNINGS="-Wall -Winline"


%install
rm -rf "%{buildroot}"
make install DESTDIR="%{buildroot}"
rm -f "%{buildroot}%{_infodir}/dir"
# Caution: We will not notice when anything new appears in docdir.
# So package maintainers need to be careful on updates.
mv "%{buildroot}%{_docdir}/%{name}-%{version}" docdir


%clean
rm -rf "%{buildroot}"


%check
make DESTDIR="%{buildroot}" check


%post
/sbin/install-info "%{_infodir}/simulavr.info.gz" "%{_infodir}/dir" || :


%preun
if [ "$1" = 0 ]; then
   /sbin/install-info --delete "%{_infodir}/simulavr.info.gz" "%{_infodir}/dir" || :
fi


%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING
%doc ChangeLog ChangeLog-2001 ChangeLog-2002 ChangeLog-2003
%doc ProjSummary README README.gdb README.opcodes TODO
%{_bindir}/simulavr
%{_bindir}/simulavr-disp
%{_bindir}/simulavr-vcd
%{_infodir}/simulavr.info.gz
%{_mandir}/man1/simulavr-disp.1.gz
%{_mandir}/man1/simulavr.1.gz


%files doc
%defattr(-,root,root,-)
%doc docdir/html
%doc docdir/internals_html
%doc docdir/simulavr-internals.pdf
%doc docdir/simulavr.pdf


%changelog
* Thu Dec 31 2009 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.1.2.6-5
- Spec file: description, comment improvements

* Wed Dec 30 2009 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.1.2.6-4
- Capitalize -doc subpackage summary
- Use generic download URL instead of specific mirror
- Use mktemp based BuildRoot
- Spec file comment cleanup

* Wed Dec 30 2009 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.1.2.6-3
- Add the final BuildRequires (texinfo-tex, texi2html)

* Wed Dec 30 2009 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.1.2.6-2
- Add sufficient BuildRequires for mock build

* Wed Dec 30 2009 Hans Ulrich Niedermann <hun@n-dimensional.de> - 0.1.2.6-1
- Initial RPM package for Fedora

